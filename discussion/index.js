//Math Operators
/*
+ sum
- difference
* product
/ quotient
% remainder

Signs to use when value of 1st number is stored in a variable. Performs operation & stores resulting value in that variable.
+= Addition
-= subtraction
*= multiplication
/= divition
%  modulo (returns 0 if no remainder)
*/
function mod(){
	//return 9+2
	//return 9-2
	//return 9*2
	//return 9/2
	//return 9%2

	let x = 10
	//return x += 2
	//return x -= 2
	//return x *= 2
	//return x /= 2
	return x %= 2


};

console.log(mod());

//assignment operator (=) is used for assigning values to variables
let x = 1;
let sum = 1;
//sum = sum + 1
 x -= 1;
 sum += 1;
console.log(sum);
console.log(x);

//Increment & Decrement

//increment

//Pre-increment - add 1 BEFORE assigning value to variable

let z = 1;
//let z = (1) + 1; before it adds value, it already has 1
let increment = ++z;
//LET Z = 1 + (1)
console.log(`Result of pre-increment: ${increment}`); //2
console.log(`Result of pre-increment: ${z}`); //2

//post increment add 1 AFTER assigning value to variable
increment = z++;
 console.log(`Result of pre-increment: ${increment}`); //2
 console.log(`Result of pre-increment: ${z}`);  //3

 //decrement

 //pre-decrement-subtracts 1, BEFORE assigning value bec most recent value of z is 3.
let decrement = --z;
console.log(`Result of pre-decrement: ${decrement}`); //2
console.log(`Result of pre-decrement: ${z}`); //2

//pst-decrement-subtracting 1 AFTER assigning of values
decrement = z--
//z= 2 - (1)
console.log(`Result of post-decrement: ${decrement}`); //2
console.log(`Result of post-decrement: ${z}`); //1

//comparison operators - boolean
//equality operator (==) compares if 2 values are equal
let juan = "juan";

console.log(1 == 1);
console.log(0 == false); //0 is off thats why it returns true
console.log(juan == "juan");

//strict equality (===)
//use case - navigating in webpages Course, Course Outline
/*if (Course == Course){
	course.html
}*/
/*if (Data == Course Outline){
	courseOutline.html
}*/
//JS is not concerned with other relative functions of data, only compares 2 values as they are;
console.log(1 === 1); //binary - on for 1, 2
console.log(1 === true); 
console.log(juan === "Juan"); 

//inequality (!=)
console.log(1 != 1);
console.log(juan != "juan");

//strict inequality (!==)
console.log(0 !== false);

//other comparison operators
/*
	< greater than
	> less than
	>= greater than or equal to
	<=less than or equal to
*/


//logical operators
/*
	AND operator (&&) - returns true if all operands are true
	true+true = true
	true+false = false
	false+true = false
	false+false = false


	OR Operator (||)
	true+true = true
	true+false = true
	false+true = true
	false+false = false
*/

//And Operator (&&)
let isLegalAge = true;
let isRegistered = true;

let allRequirementsMet = isLegalAge && isRegistered
console.log(`Result of logical AND operator: ${allRequirementsMet}`);


//OR operator (||)
allRequirementsMet = isLegalAge || isRegistered;
console.log(`Result of logical OR operator: ${allRequirementsMet}`);

//selection control structures

/*
IF Statements - execute a command if a specified condition is true
	SYNTAX:
	if (condition to be met){
	statement/command
	}
*/
let num = -1;
if (num<0) {
	console.log("Hello")
};

let value = 30;
if (value > 10 && value < 40) {
	console.log(`Welcome to zuitt`)
};

/*
IF-ELSE statement - executes commant if 1st condition returns false
	SYNTAX:
	if(condition){
	statement if true
	}
	else {
	statement if false
	};
*/

num = 5
if (num > 10) {
	console.log(`Number is greater than 10`)
}
else{
	console.log(`Number is less than 10`)
};

/*
prompt- dialog box with input keys
alert- dialog box w/o input field; used for warnings, announcements, etc.

parseInt - only accepts inputs starting with #; will convert string data type to numerical data type from a prompt
	only recognizes# & ignores alphabets (returns Not An Alphabet-NAN) 
*/

num = parseInt(prompt("Please input a number."));
if (num > 59){
	alert("Senior Age")
	console.log(num)
}
else{
	alert("Invalid Age")
	console.log(num)
};

//IF-ELSE IF- ELSE Statement - multiple separate, connected conditions
/*	if (consition){
		statement
	}
	else if (condition){
		statement
	}
	else{
		statement
	}
*/

/*
	1. Quezon
	2. Valenzuela
	3. Pasig
	4. Taguig
*/
let city = parseInt(prompt("Enter a Number"));
if (city === 1){
	alert("Welcome to Quezon City")
}
else if (city === 2){
	alert("Welcome to Valenzuela City")
}
else if (city === 3){
	alert("Welcome to Pasig City")
}
else if ( city === 4){
	alert("Welcome to Taguig City")
}
else{
	alert("Invalid Number")
};

let message = ""

function determineTyphoonIntensity(windspeed){
	if(windspeed < 30){
		return `Not a typhoon yet.`
	}
	else if (windspeed <= 61){
		return `Tropical Depression detected.`
	}
	else if (windspeed >=62 && windspeed<= 88){
		return`Tropical Storm detected.`
	}
	else if (windspeed >= 89 && windspeed <= 117){
		return`Severe Tropical Storm detected`
	}
	else{
		return `Typhoon detected`
	}
}

message = determineTyphoonIntensity(70);
console.log(message);

/*
Ternary operator - shorthanded if-else statement
SYNTAX:
(condition) ? ifTrue : ifFalse
*/

//this ternary operator is same as if-else solution below...
let ternaryResult = (1 < 18) ? true : false
console.log(`Result of ternary operator: ${ternaryResult}`);

let name;
function isOfLegalAge(){
	name = "John";
	return "You are of the age Limit"
};

function isUnderAge(){
	name = "Jane"
	return "You are under Age"
};
let age = parseInt(prompt("What is Your Age?"));
let LegalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
alert(`Result of Ternary Operator in Function: ${LegalAge} ${name}`);

//same as this if-else solution...
/*
if (age>=18){
	isOfLegalAge()
}
else{
	isUnderAge()
}
alert(`Result of Ternary Operator in Function: ${LegalAge} ${name}`);
*/


//switch statement - shorthand for if-elseif-else (2-3 conditions only) should there be a lot of conditions to be met.
//.toLowerCase - treats all user inputs as lowercase
/*
	SYNTAX
	switch (expression/parameter){
		case value1:
			statement/s;
			break
		case value2:
			statement/s;
			break
		case value3:
			statement/s;
			break
		case value4:
			statement/s;
			break
		.
		.
		.
		default:
			statements;
	}

break - signals browser that when case value has been met, no need to go further down the command.
*/
let day = prompt("What day is it today?").toLowerCase();
switch(day){
	case "sunday":
		alert("The color of the day is red");
		break;
	case "monday":
		alert("The color of the day is orange");
		break;
	case "tuesday":
		alert("The color of the day is yellow");
		break;
	case "wednesday":
		alert("The color of the day is green");
		break;
	case "thursday":
		alert("The color of the day is blue");
		break;
	case "friday":
		alert("The color of the day is violet");
		break;
	case "saturday":
		alert("The color of the day is indigo");
		break;
	default:
		alert("Please input a valid day");
};

//Try-Catch-Finally Statement
/*
like if-elseif-else statement byut used for error catching
*/
function showIntensityAlert(windspeed){
	try{
		alert(determineTyphoonIntensity(windspeed));
	}
	catch(error){
		console.log(typeof error);
		console.warn(error.message);
	}
	finally{
		alert("Intensity updates will show new alert")
	}
}
showIntensityAlert(56);
//showIntensityAlert(1e); (WITH ERROR)